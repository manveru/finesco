{
  description = "Flake for the Finesco website";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.follows = "euphenix/flake-utils";
    euphenix.url = "gitlab:manveru/euphenix/flake";
  };

  outputs = { self, euphenix, flake-utils, nixpkgs }:
    (flake-utils.lib.simpleFlake {
      inherit self nixpkgs;
      name = "site";
      preOverlays = [ euphenix.overlay ];
      overlay = ./overlay.nix;
      shell = ./shell.nix;
    }) // {
      overlay = import ./overlay.nix;
    };
}
