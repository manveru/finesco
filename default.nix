{ euphenix }:
let
  inherit (euphenix.lib) take optionalString hasPrefix nameValuePair;
  inherit (euphenix) build loadPosts sortByRecent mkPostCSS cssTag;

  withLayout = body: [ ./templates/layout.html body ];

  excludeDrafts = posts:
    euphenix.lib.filter (post: !(post.meta.draft or false)) posts;

  vars = rec {
    activeClass = route: prefix:
      optionalString (hasPrefix prefix route) "active";
    info = excludeDrafts (sortByRecent (loadPosts "/info/" ./content/info));
    blog = excludeDrafts (sortByRecent (loadPosts "/blog/" ./content/blog));
    latestBlogPosts = take 3 (sortByRecent (info ++ blog));
    liveJS = ''<script src="/js/live.js"></script>'';
    css = cssTag (mkPostCSS ./css);
  };

  route = template: title: id: {
    template = withLayout template;
    variables = vars // { inherit id title; };
  };

  mkRSS = feed:
    euphenix.mkDerivation {
      name = "rss";
      buildInputs = [ euphenix.ruby euphenix.coreutils ];
      buildCommand = ''
        mkdir -p $out
        ruby ${./scripts/rss.rb} ${
          __toFile "rss.json" (__toJSON feed)
        } > $out/feed
      '';
    };

  pageRoutes = {
    "/about/index.html" = route ./templates/about.html "会社概要" "about";
    "/blog/index.html" = route ./templates/blog.html "ブログ" "blog";
    "/column/index.html" = route ./templates/column.html "コラム" "column";
    "/contact/index.html" = route ./templates/contact.html "お問い合わせ" "contact";
    "/disclaimer/index.html" =
      route ./templates/disclaimer.html "免責事項" "disclaimer";
    "/english/index.html" = route ./templates/english.html "About us" "english";
    "/index.html" = route ./templates/home.html "Home" "home";
    "/info/index.html" = route ./templates/info.html "お知らせ" "blog";
    "/privacy/index.html" = route ./templates/privacy.html "個人情報保護方針" "privacy";
    "/rules/index.html" = route ./templates/rules.html "サイトのご利用にあたって" "rules";
    "/service/index.html" = route ./templates/service.html "サービス一覧" "service";
    "/soudan/index.html" = route ./templates/soudan.html "ご相談の流れ" "soudan";
    "/sent/en.html" =
      route ./templates/contact-sent-en.html "Thank you!" "sent";
    "/sent/jp.html" =
      route ./templates/contact-sent-jp.html "お問い合わせを受け付けました。" "sent";
  };

  blogAndInfoRoutes = __listToAttrs (map (post:
    nameValuePair post.url {
      template = withLayout ./templates/post.html;
      variables = vars // post // {
        title = post.meta.title;
        id = "blog";
      };
    }) (vars.blog ++ vars.info));

in build {
  src = ./.;
  routes = pageRoutes // blogAndInfoRoutes;
  favicon = ./static/images/favicon.svg;

  extraParts = let
    common = description: id: {
      channel = {
        inherit description;
        link = "https://www.finesco.jp/${id}";
        title = "フィネスコ株式会社 - ${description}";
        language = "ja";
        generator = "EupheNix";
      };
      items = vars.${id};
    };
  in map (feed: mkRSS feed) [ (common "ブログ" "blog") (common "お知らせ" "info") ];
}
