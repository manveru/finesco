---
date: 2019-06-12
title: 下野新聞掲載　スポーツギアさん記事
draft: true
---

今日の下野新聞。  
足利のスポーツギアさんの記事です。  
一社に一台、福利厚生の一環として、ピッチングマシン、いかがですか？  
スカッとすること請け合いです！  
（弊社は出資と私、山田が役員をしております）  
[sports-gear.co.jp](https://www.sports-gear.co.jp/)

![](/images/uploads/sports_1.jpg)
